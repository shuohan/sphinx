Welcome to Sphinx Example Project's documentation!
==================================================

Contents:

.. automodule:: my_project.code
    :members:

.. toctree::
   :maxdepth: 2

.. Indices and tables
.. ==================
.. 
.. * :ref:`genindex`
.. * :ref:`search`
.. 
.. Fork this project
.. ==================
.. 
.. * https://gitlab.com/pages/sphinx
