#!/usr/bin/env python
# -*- coding: utf-8 -*-


def func(arg1, arg2):
    """This is a func

    It tests function docstring using :func:`func`

    Args:
        arg1 (numpy.ndarray, 3D): Argument 1.
        arg2 (tuple of slice): ``arg1`` Argument 2.
    
    Returns:
        tuple, 3D: hello world

    """
    result1 = arg1 + arg2
    result2 = arg1 - arg2
    return result1, result2

if __name__ == "__main__":
    print(func(1, 2))

"""
    Returns:
        tuple:
        - **result1** (*int*) -- the first result
        - **result2** (*int*) -- the second result
"""
